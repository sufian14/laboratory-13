import java.util.*;

public class Queue {
	public static void main(String[] args) {
		//comparator with the length
		PriorityQueue<int[]> pq = new PriorityQueue<int[]>(5, (a, b) -> a.length - b.length);
		
		int[] a = new int[] { 1, 2 };
		int[] b = { 1, 2, 3 };
		int[] c = new int[] { 3 };
		pq.add(a);
		pq.add(b);
		pq.add(c);
		
		System.out.println("The priority queue: ");
		for (int[] i : pq)
	    {
	        System.out.println(Arrays.toString(i));
	    }
		
		pq.poll(); //remove the first element 
		int[] x = pq.peek(); //return the first element
		System.out.println("First elemet after remove: " + Arrays.toString(x));


	}
}